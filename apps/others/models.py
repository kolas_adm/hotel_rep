from django.db import models
from ckeditor.fields import RichTextField
import datetime
from django.utils.translation import ugettext_lazy as _


class Stock(models.Model):

    title_ua = models.CharField(max_length=255, blank=False, null=False)
    description_ua = RichTextField()

    title_en = models.CharField(max_length=255, blank=True, null=True, default='')
    description_en = RichTextField(default='')

    title_he = models.CharField(max_length=255, blank=True, null=True, default='')
    description_he = RichTextField(default='')
    timestamp = models.DateField(default=datetime.date.today)

    class Meta:
        verbose_name = _('Stock')
        verbose_name_plural = _('Stocks')

    def __str__(self):
        return "%s" % self.title_ua

    def image(self):
        for img in self.images.all():
            return img.image.url
        return None


class StockImage(models.Model):
    stock = models.ForeignKey(Stock, models.CASCADE, verbose_name=_('stock'), related_name='images')
    image = models.ImageField(upload_to='images')

    class Meta:
        verbose_name = _('Stock image')
        verbose_name_plural = _('Stock images')


class Carousel(models.Model):
    GALLERY = 'GL'
    RESTAURANT = 'RS'
    OTHER = 'OT'

    CAROUSEL_TYPE_CHOICE = (
        (GALLERY, 'Gallery type'),
        (RESTAURANT, 'Restaurant type'),
        (OTHER, 'Other type'),
    )
    name = models.CharField(max_length=255, blank=False, null=False)
    gallery_type = models.CharField(
        _('Type of gallery'),
        max_length=2,
        choices=CAROUSEL_TYPE_CHOICE,
        default=OTHER,
    )

    class Meta:
        verbose_name = _("Gallery")
        verbose_name_plural = _("Galleries")

    def __str__(self):
        return "%s" % self.name

    def image(self):
        for img in self.carousels.all():
            return img.image.url
        return None


class CarouselImage(models.Model):
    carousel = models.ForeignKey(Carousel, models.CASCADE, verbose_name=_('carousel'), related_name='carousels')
    image = models.ImageField(upload_to='images')

    class Meta:
        verbose_name = _('Carousel image')
        verbose_name_plural = _('CArousel images')


class Locales(models.Model):
    name = models.CharField('назва', max_length=255, unique=True)
    ua = models.TextField('укр')
    en = models.TextField('eng')
    he = models.TextField('he')

    class Meta:
        verbose_name = _('Locale')
        verbose_name_plural = _('Locales')

    def __str__(self):
        return self.ua

    def to_dict(self):
        return {
            'UA': self.ua,
            'EN': self.en,
            'HE': self.he,
        }

