from rest_framework import serializers
from .models import Stock, CarouselImage


class StocksListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stock

        fields = (
            'id',
            'title_ua',
            'title_en',
            'title_he',
            'description_ua',
            'description_en',
            'description_he',
            'timestamp',
            'image',
        )


class StocksDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stock

        fields = (
            'description_ua',
            'description_en',
            'description_he',
            'image',
        )


class CarouselDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = CarouselImage

        fields = (
            'image',
        )
