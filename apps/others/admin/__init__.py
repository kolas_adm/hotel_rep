from django.contrib import admin

from ..models import Carousel, Stock, Locales
from .inlines import CarouselImageInline, StockImageInline


@admin.register(Carousel)
class CarouselRoomAdmin(admin.ModelAdmin):
    list_display = ('name', 'gallery_type',)
    inlines = (CarouselImageInline,)


@admin.register(Stock)
class StocksAdmin(admin.ModelAdmin):
    list_display = ('title_ua', 'title_en', 'title_he', 'timestamp')
    inlines = (StockImageInline,)


@admin.register(Locales)
class LocalesAdmin(admin.ModelAdmin):
    def get_readonly_fields(self, request, obj=None):
        if obj is None:
            return ()
        return 'name',
