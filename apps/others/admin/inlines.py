from django.contrib.admin import TabularInline
from django.utils.safestring import mark_safe

from ..models import CarouselImage, StockImage


class CarouselImageInline(TabularInline):
    model = CarouselImage
    fields = ('image', 'image_tag')
    readonly_fields = ('image_tag',)

    def image_tag(self, obj):
        if not obj.id:
            return ''
        return mark_safe('<img src="/directory/%s" width="128" height="128" />' % obj.image)

    image_tag.short_description = 'Image'
    image_tag.allow_tags = True


class StockImageInline(TabularInline):
    model = StockImage
    fields = ('image', 'image_tag')
    readonly_fields = ('image_tag',)

    def image_tag(self, obj):
        if not obj.id:
            return ''
        return mark_safe('<img src="/directory/%s" width="128" height="128" />' % obj.image)

    image_tag.short_description = 'Image'
    image_tag.allow_tags = True
