from django.urls import path

from . import views

urlpatterns = [
    path('stocks/', views.StocksListView.as_view()),
    path('stocks/<slug:pk>/', views.StocksDetailView.as_view()),
    path('carousel/<str:type>/', views.CarouselDetailView.as_view()),
    path('locales/', views.get_locales),
]
