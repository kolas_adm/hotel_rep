# Generated by Django 2.1.4 on 2019-02-26 06:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('others', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Carousel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('gallery_type', models.CharField(choices=[('GL', 'Gallery type'), ('RS', 'Restaurant type'), ('OT', 'Other type')], default='OT', max_length=2, verbose_name='Type of gallery')),
            ],
            options={
                'verbose_name': 'Gallery',
                'verbose_name_plural': 'Galleries',
            },
        ),
        migrations.CreateModel(
            name='CarouselImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='images')),
                ('carousel', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='carousels', to='others.Carousel', verbose_name='carousel')),
            ],
            options={
                'verbose_name': 'Carousel image',
                'verbose_name_plural': 'CArousel images',
            },
        ),
        migrations.RenameModel(
            old_name='RoomImage',
            new_name='StockImage',
        ),
        migrations.RenameField(
            model_name='stockimage',
            old_name='room',
            new_name='stock',
        ),
    ]
