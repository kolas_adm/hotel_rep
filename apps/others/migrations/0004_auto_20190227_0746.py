# Generated by Django 2.1.4 on 2019-02-27 07:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('others', '0003_locales'),
    ]

    operations = [
        migrations.AlterField(
            model_name='locales',
            name='name',
            field=models.CharField(max_length=255, unique=True, verbose_name='назва'),
        ),
    ]
