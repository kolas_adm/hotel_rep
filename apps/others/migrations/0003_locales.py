# Generated by Django 2.1.4 on 2019-02-27 07:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('others', '0002_auto_20190226_0644'),
    ]

    operations = [
        migrations.CreateModel(
            name='Locales',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='назва')),
                ('ua', models.TextField(verbose_name='укр')),
                ('en', models.TextField(verbose_name='eng')),
                ('he', models.TextField(verbose_name='he')),
            ],
            options={
                'verbose_name': 'Locale',
                'verbose_name_plural': 'Locales',
            },
        ),
    ]
