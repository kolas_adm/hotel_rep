from .serializers import StocksDetailSerializer, StocksListSerializer, CarouselDetailSerializer
from .models import Stock, Carousel, Locales
from apps.hotel_room.pagination import SmallPagesPagination
from rest_framework.generics import ListCreateAPIView
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view


class StocksListView(ListCreateAPIView):
    """
    API View to get list of all News
    Allow only GET method
    """
    http_method_names = ['get']
    queryset = Stock.objects.order_by('-timestamp', '-id')
    serializer_class = StocksListSerializer
    pagination_class = SmallPagesPagination


class StocksDetailView(APIView):
    """
    API View for getting detail info of News
    Allowed only GET method
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, pk, format=None):
        try:
            news = Stock.objects.get(id=pk)
            serialier = StocksDetailSerializer(news)
            return Response(serialier.data)
        except (Stock.DoesNotExist, IndexError):
            return Response(status=404)


class CarouselDetailView(APIView):
    """
    API View for getting detail info of News
    Allowed only GET method
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, type, format=None):
        if type == '1':
            try:
                gallery = Carousel.objects.prefetch_related('carousels').get(gallery_type=Carousel.GALLERY)
                images = gallery.carousels.all()
                serializer = CarouselDetailSerializer(images, many=True)
                return Response(serializer.data)
            except (Carousel.DoesNotExist, IndexError):
                return Response(status=404)
        elif type == '2':
            try:
                gallery = Carousel.objects.prefetch_related('carousels').get(gallery_type=Carousel.RESTAURANT)
                images = gallery.carousels.all()
                serializer = CarouselDetailSerializer(images, many=True)
                print(serializer.data)
                return Response(serializer.data)
            except (Carousel.DoesNotExist, IndexError):
                return Response(status=404)


@api_view(['GET'])
def get_locales(request):
    locales = {locale.name: locale.to_dict() for locale in Locales.objects.all()}
    return Response(locales)
