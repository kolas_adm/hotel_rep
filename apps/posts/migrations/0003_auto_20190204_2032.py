# Generated by Django 2.1.4 on 2019-02-04 20:32

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0002_flatpage'),
    ]

    operations = [
        migrations.AlterField(
            model_name='flatpage',
            name='content_EN',
            field=ckeditor.fields.RichTextField(),
        ),
        migrations.AlterField(
            model_name='flatpage',
            name='content_HE',
            field=ckeditor.fields.RichTextField(),
        ),
        migrations.AlterField(
            model_name='flatpage',
            name='content_UA',
            field=ckeditor.fields.RichTextField(),
        ),
    ]
