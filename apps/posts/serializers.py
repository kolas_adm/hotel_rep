from rest_framework import serializers
from apps.posts.models import Post


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = (
            'rating_status',
            'user',
            'full_name_user',
            'advantages',
            'limitations',
            'description',
        )
        read_only_fields = ('full_name_user',)

    def create(self, validated_data):
        return Post.objects.create(**validated_data)
