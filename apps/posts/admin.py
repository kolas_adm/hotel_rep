from django.contrib import admin
from .models import Post, FlatPage


@admin.register(Post)
class NewsAdmin(admin.ModelAdmin):
    list_display = ('rating_status', 'user')


@admin.register(FlatPage)
class NewsAdmin(admin.ModelAdmin):
    list_display = ('name', 'url')
