from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from rest_framework.response import Response
from rest_framework import permissions, status
from rest_framework.generics import ListCreateAPIView, CreateAPIView
from rest_framework.views import APIView

from apps.hotel_room.pagination import SmallPagesPagination
from apps.posts.models import Post, FlatPage
from apps.posts.serializers import PostSerializer
from apps.users.models import User


class PostsListView(ListCreateAPIView):
    """
    API View to get list of all Posts
    Allow only GET method
    """
    http_method_names = ['get']
    queryset = Post.objects.select_related('user').order_by('-id')
    serializer_class = PostSerializer
    # pagination_class = SmallPagesPagination


class CreatePostView(CreateAPIView):
    """
    Test permissions - can use anonymius user too
        to make permission only for authenticated user
        uncomment line №41 at posts/views.py
    Permissions - only authenticated user can use
    rating_status - should be a string (ex. '1', '2', '3', '4', '5')
    user - should be a string with user id (ex. '11f7fb97-56c1-4b74-8e01-7fb73585ec17')
    advantages - any string
    limitations - any string
    description - any string
    """
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = (permissions.IsAuthenticated,)

    http_method_names = ['post']

    def post(self, request, format=None, **kwargs):
        data = request.data
        data['user'] = request.user.id
        serializer = PostSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def flatpage(request, url):
    try:
        page = FlatPage.objects.get(url=url)
    except FlatPage.DoesNotExist:
        return JsonResponse('Page does not exist', status=404)
    return JsonResponse({
        'UA': page.content_UA,
        'EN': page.content_EN,
        'HE': page.content_HE,
    })
