from django.db import models
from django.utils.translation import ugettext_lazy as _

from ckeditor.fields import RichTextField

from apps.users.models import User


class Post(models.Model):
    # TODO можливо додати якийсь флаг типу 'confirmed' для валідації комента адміном? шоб матюків не понаписували))
    ONE_START = 1
    TWO_STARTS = 2
    THREE_STARS = 3
    FOUR_STARS = 4
    FIVE_STARS = 5

    POST_STARS_CHOICES = (
        (ONE_START, 'One star'),
        (TWO_STARTS, 'Two star'),
        (THREE_STARS, 'Three star'),
        (FOUR_STARS, 'Four star'),
        (FIVE_STARS, 'Five star'),
    )

    rating_status = models.SmallIntegerField(
        _('Booking status'),
        choices=POST_STARS_CHOICES,
        default=FIVE_STARS,
    )

    user = models.ForeignKey(User, models.PROTECT, verbose_name=_('user'), related_name='posts',
                             blank=False, null=False)

    advantages = models.TextField(null=True, blank=True)
    limitations = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    def full_name_user(self):
        return self.user.full_name


class FlatPage(models.Model):
    name = models.CharField(max_length=255)
    url = models.CharField(max_length=100, db_index=True)
    content_UA = RichTextField()
    content_EN = RichTextField()
    content_HE = RichTextField()
