from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('posts/', views.PostsListView.as_view()),
    path('posts/create/', views.CreatePostView.as_view()),
    path('page/<str:url>/', views.flatpage)
]
