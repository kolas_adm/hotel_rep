from apps.users.models import User
from apps.users.serializers import UserSerializer
# Create your views here.
from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework import generics
from django.contrib.sitemaps import Sitemap


class UserListView(LoginRequiredMixin, generics.ListCreateAPIView):
    """
    API View to get all custom users list
    Allow only for authorized users
    """
    http_method_names = ['get']
    queryset = User.objects.all()
    serializer_class = UserSerializer


class DetailUserView(LoginRequiredMixin, generics.RetrieveAPIView):
    """
    Register new user
    """
    http_method_names = ['get']
    queryset = User.objects.all()
    serializer_class = UserSerializer


class PagesSitemap(Sitemap):
    changefreq = 'weekly'

    def items(self):
        return (
            '',
            '/booking',
            '/stocks',
            '/hotel',
            '/restaurant',
            '/gallery',
            '/news',
            '/comments',
            '/history',
            '/plan',
            '/contacts',
        )

    def location(self, item):
        return item


sitemaps = {
    'pages': PagesSitemap,
}

