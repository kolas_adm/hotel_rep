from django.urls import include, path

from . import views

urlpatterns = [
    path('get-all-users', views.UserListView.as_view()),
    path('rest-auth/', include('rest_auth.urls')),
]