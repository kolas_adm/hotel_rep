import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _


# Create your models here.
class User(AbstractUser):
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ('username', 'last_name', 'first_name', )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.EmailField(null=False, unique=True)
    username = models.CharField(max_length=3000, unique=False)

    first_name = models.CharField(_('first name'), max_length=100, blank=True)
    last_name = models.CharField(_('last name'), max_length=100, blank=True)

    phone = models.CharField(max_length=30, null=True, blank=True)
    country = models.CharField(null=True, blank=True, max_length=100)

    @property
    def full_name(self):
        if self.first_name and self.last_name:
            return '{} {}.'.format(
                self.first_name,
                self.last_name[0].upper()
            )
        return self.username

    def __str__(self):
        return self.username
