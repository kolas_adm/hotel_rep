import os
import uuid
import datetime
from builtins import StopIteration
from ckeditor.fields import RichTextField

from django.db import models
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from apps.users.models import User


# Create your models here.
def get_date_next_week():
    """
    Getting date now and plus 7 days. Need to get date after 7 days to use in date of departure.
    As default used one week accommodation
    :return: datetime
    """
    date_time_now = datetime.datetime.now()
    return date_time_now + datetime.timedelta(days=7)


class HotelRoom(models.Model):
    # At that moment there will be only lux and standart types.
    STANDART = 'ST'
    LUX = 'LX'

    # Means count of bedrooms. Like in one HOTEL room we have two rooms. One living room and one bedroom, or
    # two bedrooms, or something else. Do not know how better explain this in English.
    TWO_ROOMS = '2'
    THREE_ROOMS = '3'

    # I do not sure of this count, but think it is true, later will check it
    LODGERS_THREE = '3'
    LODGERS_SIX = '6'

    ROOM_TYPE_CHOICES = (
        (STANDART, 'Standart'),
        (LUX, 'Lux'),
    )

    ROOM_COUNT_CHOICE = (
        (TWO_ROOMS, 'Two bedrooms'),
        (THREE_ROOMS, 'Three bedrooms'),
    )

    LODGERS_COUNT_CHOICE = (
        (LODGERS_THREE, 'Three lodgers'),
        (LODGERS_SIX, 'Six lodgers'),
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    room_type = models.CharField(
        _('Type of room'),
        max_length=2,
        choices=ROOM_TYPE_CHOICES,
        default=STANDART,
    )
    room_count = models.CharField(
        _('Bedrooms count'),
        max_length=1,
        choices=ROOM_COUNT_CHOICE,
        default=TWO_ROOMS
    )
    lodgers_count = models.CharField(
        _('Count of the lodgers'),
        max_length=1,
        choices=LODGERS_COUNT_CHOICE,
        default=LODGERS_THREE
    )

    price = models.PositiveIntegerField(_('Price per one day'), default=100, blank=False, null=True)
    price_two_people = models.PositiveIntegerField(_('Price per one day for two people'), default=110, blank=False, null=True)
    price_three_people = models.PositiveIntegerField(_('Price per one day for three people'), default=110, blank=False, null=True)
    room_number = models.PositiveIntegerField(_('room number'), unique=True)

    class Meta:
        verbose_name = _("Hotel room")
        verbose_name_plural = _("Hotel rooms")

    def __str__(self):
        return "%s" % self.room_number

    def image(self):
        for img in self.images.all():
            return img.image.url
        return None


class RoomImage(models.Model):
    room = models.ForeignKey(HotelRoom, models.CASCADE, verbose_name=_('room'), related_name='images')
    image = models.ImageField(upload_to='images')

    class Meta:
        verbose_name = _('Room image')
        verbose_name_plural = _('Room images')


@receiver(models.signals.post_delete, sender=RoomImage)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Delete file from filesystem when corresponding `RoomImage` object is deleted.
    """
    if os.path.isfile(instance.image.path):
        os.remove(instance.image.path)


@receiver(models.signals.pre_save, sender=RoomImage)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """
    Delete old file from filesystem when corresponding `RoomImage` object is updated with new file.
    """
    if not instance.pk:
        return False

    try:
        old_file = RoomImage.objects.get(pk=instance.pk).image
    except RoomImage.DoesNotExist:
        return False

    new_file = instance.image
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


class Booking(models.Model):
    PENDING = 1
    ACCEPTED = 2
    DENIED = 3

    ARRIVED = 4
    LEFT = 5

    PAYED = 6
    FINISHED = 7
    CANCELLED = 8
    BOOKING_STATUS_CHOICES = (
        (PENDING, 'Pending'),
        (ACCEPTED, 'Accepted'),
        (DENIED, 'Denied'),
        (PAYED, 'Payed'),
        (CANCELLED, 'Cancelled'),
        (ARRIVED, 'Arrived'),
        (LEFT, 'Left'),
        (FINISHED, 'Finished'),
    )
    booking_status = models.SmallIntegerField(
        _('Booking status'),
        choices=BOOKING_STATUS_CHOICES,
        default=PENDING,
    )
    room = models.ForeignKey(HotelRoom, models.PROTECT, verbose_name=_('room'), related_name='bookings')
    # I think we need user field as optional,
    # because if anonymous user will make booking we do not need use any relation,
    # we just need to get first name, last name, etc. fields from front-end
    # Actually now, we do not need any user.
    # It doesn't have any sense to register user until there will not
    # any sales or something like this.
    user = models.ForeignKey(User, models.PROTECT, verbose_name=_('user'), related_name='bookings',
                             blank=True, null=True)
    arrival_date = models.DateField(_('arrival_date'), db_index=True)
    departure_date = models.DateField(_('departure_date'), db_index=True)
    booking_timestamp = models.DateTimeField(default=datetime.datetime.now)
    arrived = models.BooleanField(default=False)
    left = models.BooleanField(default=False)
    comment = models.TextField(null=True, blank=True)
    person_count = models.SmallIntegerField(default=1)

    # Next fields we will get from user object or from front-end form
    first_name = models.CharField(_('first name'), max_length=100, blank=False,
                                  default='')
    last_name = models.CharField(_('last name'), max_length=100, blank=False,
                                 default='')
    phone = models.CharField(max_length=30, null=True, blank=True,
                             default='')
    country = models.CharField(null=True, blank=True, max_length=100,
                               default='')

    class Meta:
        verbose_name = _('Room booking')
        verbose_name_plural = _('Room bookings')
        indexes = [
            models.Index(fields=('room', 'arrival_date', 'departure_date')),
        ]

    def save(self, *args, **kwargs):
        #  TODO add server-side validation to prevent multiple room booking in same dates.
        super().save(*args, **kwargs)

    def switch_arrived_and_left(self):
        if self.arrived is True:
            self.left = False

        if self.left is True:
            self.arrived = False

    def get_time_of_stay(self):
        return self.departure_date.day - self.arrival_date.day

    def get_time_of_stay_datetime_list(self):
        """
        Need this method to getting list of living days
        Use it for declaring new booking for this room,
        if booking date is in this list. Need to convert
        checking date to string format strftime('%Y/%m/%d')
        :return: list of strings with stayaing dates
        """
        stay_period = self.get_time_of_stay()
        stay_period_datetime_list = []
        stay_period_string_list = []
        for number in list(range(1, stay_period)):
            date_of_stay = self.arrival_date + datetime.timedelta(days=number)
            stay_period_datetime_list.append(date_of_stay)
        for date in stay_period_datetime_list:
            stay_period_string_list.append(date.strftime("%Y-%m-%d"))
        stay_period_string_list.append(self.arrival_date.strftime("%Y-%m-%d"))
        stay_period_string_list.append(self.departure_date.strftime("%Y-%m-%d"))
        return stay_period_string_list

    @classmethod
    def check_booking_date(cls, arrive_date, left_date, room_number):
        """
        Checking if we can make new booking to given room,
        with given dates
        :param arrive_date:
        :param left_date:
        :param room_number:
        :return: True - if we can and False - if not
        """
        # Here gets list of string with given living time period
        # includes dates of arrive and left
        left_date = datetime.datetime.strptime(left_date, "%Y-%m-%d")
        arrive_date = datetime.datetime.strptime(arrive_date, "%Y-%m-%d")
        days_of_living = left_date.day - arrive_date.day
        stay_period_datetime_list = []
        stay_period_string_list = []
        for number in list(range(1, days_of_living)):
            date_of_stay = arrive_date + datetime.timedelta(days=number)
            stay_period_datetime_list.append(date_of_stay)
        for date in stay_period_datetime_list:
            stay_period_string_list.append(date.strftime("%Y-%m-%d"))
        str_arrive_date = arrive_date.strftime('%Y-%m-%d')
        str_left_date = left_date.strftime('%Y-%m-%d')
        stay_period_string_list.append(str_arrive_date)
        stay_period_string_list.append(str_left_date)

        # Get HotelRoom instance with a room which need to book
        room = HotelRoom.objects.get(room_number=room_number)
        # Get all bookings with this HotelRoom
        query_set = Booking.objects.filter(room=room.id)
        if len(query_set) is 0:
            return True

        for booking in query_set:
            # here compare given live period with live periods on
            # bookings queryset.
            # If there is no matches and status is not FINISHED or CANCELED - then we can make new booking.
            living_time = booking.get_time_of_stay_datetime_list()
            compare_result = set(living_time) & set(stay_period_string_list)
            if len(compare_result) is not 0:
                if booking.booking_status is 7 or booking.booking_status is 8:
                    return True
                return False
            else:
                return True

    # @classmethod
    # def get_free_rooms(cls, arrive_date, left_date):
    #
    #
    #     query_set = Booking.objects.filter(arrival_date=stay_period_datetime_list,
    #                                        departure_date__in=stay_period_datetime_list)
    #
    #     print(query_set)


class News(models.Model):

    title_ua = models.CharField(max_length=255, blank=False, null=False)
    description_ua = RichTextField()

    title_en = models.CharField(max_length=255, blank=True, null=True, default='')
    description_en = RichTextField(default='')

    title_he = models.CharField(max_length=255, blank=True, null=True, default='')
    description_he = RichTextField(default='')
    timestamp = models.DateField(default=datetime.date.today)

    class Meta:
        verbose_name = _('News')
        verbose_name_plural = _('News')

    def __str__(self):
        return "%s" % self.title_ua

