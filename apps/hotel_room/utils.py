import datetime
from builtins import ValueError


def validate_date(date_str):
    try:
        datetime.datetime.strptime(date_str, "%Y-%m-%d")
        return True
    except ValueError:
        raise ValueError("Incorrect data format, should be YYYY-MM-DD")


def get_filename(filename):
    return filename.upper()
