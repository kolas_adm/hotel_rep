from django.apps import AppConfig


class HotelRoomConfig(AppConfig):
    name = 'hotel_room'
