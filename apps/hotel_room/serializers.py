from rest_framework import serializers
from rest_framework.response import Response

from .models import HotelRoom, Booking, News, RoomImage


class HotelRoomSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(read_only=True)

    class Meta:
        model = HotelRoom
        fields = (
            'id',
            'room_type',
            'room_count',
            'lodgers_count',
            'price',
            'price_two_people',
            'price_three_people',
            'room_number',
            'image',
            # 'arrival_date',
            # 'date_of_departure',
            # 'arrived',
            # 'left',
            # 'booked',
            # 'book_date'
        )


class HotelRoomImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = RoomImage

        fields = (
            'image',
        )

class BookingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Booking

        fields = (
            'room',
            'arrival_date',
            'departure_date',
            'comment',
            'person_count',
            'first_name',
            'last_name',
            'phone',
            'country',
        )

    def create(self, validated_data):
        return Booking.objects.create(**validated_data)


class NewsDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = News

        fields = (
            'description_ua',
            'description_en',
            'description_he',
        )


class NewsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = News

        fields = (
            'id',
            'title_ua',
            'title_en',
            'title_he',
            'timestamp',
        )
