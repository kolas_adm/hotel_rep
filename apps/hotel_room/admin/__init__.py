from django.contrib import admin

from ..models import HotelRoom, Booking, News
from .inlines import RoomImageInline


@admin.register(HotelRoom)
class HotelRoomAdmin(admin.ModelAdmin):
    inlines = (RoomImageInline,)


@admin.register(Booking)
class BookingAdmin(admin.ModelAdmin):
    list_display = (
        'room', 'user', 'arrival_date', 'departure_date', 'booking_timestamp', 'booking_status', 'arrived', 'left')
    list_filter = ('room',)

    def get_queryset(self, request):
        return super().get_queryset(request).select_related('room', 'user')


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    list_display = ('title_ua',)
    #
    # def save_model(self, request, obj, form, change):
    #     News.objects.all().delete()
    #     for i in range(40):
    #         News.objects.create(
    #             title_ua='Новина %s' % i,
    #             title_en='news %s' % i,
    #             title_he='חדשות %s' % i,
    #             description_ua='<p>текст новини %s</p>' % i,
    #             description_en='<p>news text %s</p>' % i,
    #             description_he='<p>%s חדשותחדשות</p>' % i,
    #         )
