from django.urls import path

from . import views

urlpatterns = [
    path('rooms/', views.HotelRoomListView.as_view()),
    path('rooms/<slug:pk>/', views.HotelRoomDetailView.as_view()),
    path('rooms/images/<slug:pk>/', views.HotelRoomGetImagesView.as_view()),
    path('rooms/update/<slug:room>/', views.HotelRoomUpdateView.as_view()),
    path('booking/get/<str:date_arr>/<str:date_left>/', views.BookingGetFreeRoomsView.as_view({'get': 'list'})),
    path('booking/create/', views.CreateBookingView.as_view()),
    path('news/', views.NewsListView.as_view()),
    path('news/<slug:pk>/', views.NewsDetailView.as_view()),
]
