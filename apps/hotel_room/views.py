from itertools import chain

from django.contrib.auth.models import AnonymousUser
from django.db.models import QuerySet
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render

# Create your views here.
from rest_framework import status, permissions, viewsets, mixins
from rest_framework.decorators import list_route
from rest_framework.generics import UpdateAPIView, ListCreateAPIView, GenericAPIView, ListAPIView, RetrieveAPIView
from rest_framework.mixins import UpdateModelMixin
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.utils import json
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from apps.hotel_room.pagination import SmallPagesPagination
from apps.hotel_room.utils import validate_date
from apps.users.views import DetailUserView
from hotel.local_settings import EMAIL_HOST_USER
from .models import HotelRoom, Booking, News
from .serializers import HotelRoomSerializer, BookingSerializer, NewsDetailSerializer, NewsListSerializer, \
    HotelRoomImageSerializer


class HotelRoomListView(ListCreateAPIView):
    """
    API View to get list of room OR make updates
    Allow only GET method
    """
    http_method_names = ['get']
    queryset = HotelRoom.objects.prefetch_related('images').all()
    serializer_class = HotelRoomSerializer


class HotelRoomDetailView(APIView):
    """
    API View for getting detail info of HotelRoom queries
    Allowed only GET method
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, pk, format=None):
        room = HotelRoom.objects.get(id=pk)
        serializer = HotelRoomSerializer(room)
        return Response(serializer.data)


class HotelRoomUpdateView(UpdateModelMixin, GenericAPIView):
    """
    API View for updating HotelRoom queries
    Allow only PUT method
    """
    permission_classes = (permissions.AllowAny,)
    queryset = HotelRoom.objects.all()
    serializer_class = HotelRoomSerializer

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)


class HotelRoomGetImagesView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, pk, format=None):
        try:
            gallery = HotelRoom.objects.prefetch_related('images').get(id=pk)
            images = gallery.images.all()
            serializer = HotelRoomImageSerializer(images, many=True)
            return Response(serializer.data)
        except (HotelRoom.DoesNotExist, IndexError):
            return Response(status=404)


class NewsListView(ListCreateAPIView):
    """
    API View to get list of all News
    Allow only GET method
    """
    http_method_names = ['get']
    queryset = News.objects.order_by('-timestamp', '-id')
    serializer_class = NewsListSerializer
    pagination_class = SmallPagesPagination


class NewsDetailView(APIView):
    """
    API View for getting detail info of News
    Allowed only GET method
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, pk, format=None):
        try:
            news = News.objects.get(id=pk)
            serialier = NewsDetailSerializer(news)
            return Response(serialier.data)
        except (News.DoesNotExist, IndexError):
            return Response(status=404)


class BookingGetFreeRoomsView(viewsets.ModelViewSet):
    serializer_class = HotelRoomSerializer
    queryset = HotelRoom.objects.all()

    def get(self):
        query_set = HotelRoom.objects.all()
        arr = ''
        left = ''
        kwargs_arr = self.kwargs['date_arr'].split('.')
        kwargs_left = self.kwargs['date_left'].split('.')
        arr = kwargs_arr[1] + '-' + kwargs_arr[0] + '-' + kwargs_arr[2]
        left = kwargs_left[1] + '-' + kwargs_left[0] + '-' + kwargs_left[2]
        objects_list = []
        booking_bool_list = []
        for qs in query_set:
            booking_bool = Booking().check_booking_date(
                arr,
                left,
                qs.room_number,
            )
            if booking_bool:
                objects_list.append(qs)
        print(objects_list)
        return objects_list


class CreateBookingView(ListCreateAPIView):
    queryset = Booking.objects.all()
    serializer_class = BookingSerializer

    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = BookingSerializer
        arrival_date = None
        departure_date = None

        room = HotelRoom.objects.get(room_number=int(data['room']))
        # TODO Add validation for not AnonymousUser, how to get first name, last name and so on.
        # TODO We can do that at front-end
        # TODO Fill all fields, or hide them.
        # if type(request.user) is AnonymousUser:
        arr_date = request.data['arrival_date']
        arr_date = arr_date.split('.')
        arrival_date = arr_date[2] + '-' + arr_date[1] + '-' + arr_date[0]
        valid_date_arr = False
        valid_date_left = False

        left_date = request.data['departure_date']
        left_date = left_date.split('.')
        departure_date = left_date[2] + '-' + left_date[1] + '-' + left_date[0]
        if validate_date(arrival_date):
            valid_date_arr = True

        if validate_date(departure_date):
            valid_date_left = True

        if valid_date_arr and valid_date_left:
            booking_bool = Booking().check_booking_date(arrival_date, departure_date, room.room_number)
        if booking_bool:
            try:
                find_booking = Booking.objects.get(room=room, arrival_date=arrival_date, departure_date=departure_date)
            except Booking.DoesNotExist:
                find_booking = None
            if not find_booking:
                first_name = request.data['first_name']
                last_name = request.data['last_name']
                phone = request.data['phone']
                country = request.data['country']
                person_count = None
                if 3 >= int(data['person_count']) > 0:
                    person_count = request.data['person_count']
                item = dict()
                item['room'] = room
                item['arrival_date'] = arrival_date
                item['departure_date'] = departure_date
                item['first_name'] = first_name
                item['last_name'] = last_name
                item['phone'] = phone
                item['country'] = country
                item['person_count'] = person_count
                if serializer.is_valid:
                    serializer().create(item)
                from django.core.mail import EmailMessage
                body = 'Dear, {0} {1},\n' \
                       'Thanks for you booking in our hotel.\n' \
                       'You have booked a room during next dates {2} - {3}\n' \
                       'If you have any questions please ask us at hotel.melania@gmail.com\n' \
                       'or call us +380-93-48-148-93\n' \
                       'Wish you a good day.\n' \
                       'Hotel "Melania" Uman team.\n'.format(
                    item.get('first_name'),
                    item.get('last_name'),
                    item.get('arrival_date'),
                    item.get('departure_date')
                )
                email = EmailMessage('Hotel room booking', body, to=[data['email']])
                email.send()

                body_to_admin = 'User with first name {0} and last name {1}\n' \
                                'have been made booking for room {2}.\n' \
                                'Dates: arrive - {3}\n' \
                                'left - {4}\n' \
                                'User contacts:\n' \
                                'phone - {5};\n' \
                                'county - {6};\n' \
                                'email - {7};\n' \
                                'person count - {8}\n'.format(
                    item.get('first_name'),
                    item.get('last_name'),
                    item.get('room'),
                    item.get('arrival_date'),
                    item.get('departure_date'),
                    item.get('phone'),
                    item.get('country'),
                    item.get('email'),
                    item.get('person_count')
                )
                email_to_admin = EmailMessage('New booking', body_to_admin, to=[EMAIL_HOST_USER])
                email_to_admin.send()
                return HttpResponse(status=200)
            else:
                return HttpResponse(status=500, content={'You can not book this room for given dates.'})
        else:
            return HttpResponse(status=500, content={'You can not book this room for given dates.'})
