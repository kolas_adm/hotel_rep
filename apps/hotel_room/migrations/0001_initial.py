# Generated by Django 2.1.4 on 2018-12-25 17:13

import datetime
from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='HotelRoom',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('room_type', models.CharField(choices=[('ST', 'Standart'), ('LX', 'Lux')], default='ST', max_length=2)),
                ('room_count', models.CharField(choices=[('2', 'Two bedrooms'), ('3', 'Three bedrooms')], default='2', max_length=1)),
                ('lodgers_count', models.CharField(choices=[('3', 'Three lodgers'), ('6', 'Six lodgers')], default='3', max_length=1)),
                ('price', models.PositiveIntegerField(default=100, null=True)),
                ('room_number', models.PositiveIntegerField(null=True)),
                ('arrival_date', models.DateField(blank=True, default=datetime.datetime.now)),
                ('date_of_departure', models.DateField(blank=True, default=datetime.datetime(2019, 1, 1, 17, 13, 49, 911261))),
                ('arrived', models.BooleanField(default=False)),
                ('left', models.BooleanField(default=False)),
                ('booked', models.BooleanField(default=False)),
                ('book_date', models.DateField(blank=True, default=datetime.datetime.now)),
            ],
        ),
    ]
