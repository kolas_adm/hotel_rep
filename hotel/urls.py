"""hotel URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from rest_framework_swagger.views import get_swagger_view
from django.contrib.sitemaps.views import sitemap

from apps.users.views import PagesSitemap

schema_view = get_swagger_view(title='Hotel reservation API')

sitemaps = {
    'static': PagesSitemap,
}

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', schema_view),
    path('users/', include('apps.users.urls')),
    path('rooms/', include('apps.hotel_room.urls')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('posts/', include('apps.posts.urls')),
    path('stocks/', include('apps.others.urls')),
    path('gallery/', include('apps.others.urls')),

    # facebook
    path('auth/social/', include('rest_framework_social_oauth2.urls')),
    path('oauth2/', include('oauth2_provider.urls')),

    path('sitemap.xml', sitemap, {'sitemaps': sitemaps},
         name='django.contrib.sitemaps.views.sitemap')
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += path('', include('apps.base.urls')),
