import React from 'react';

import Header from '../Header/Header';
import './Hotel.css';

export default class History extends React.Component {
    state = {
        content: null,
    };

    componentDidMount() {
        fetch('/posts/page/hotel/')
            .then(response => response.json())
            .then(response => this.setState({content: response}));
    }

    render() {
        const {lang, locale} = this.props;
        const content = this.state.content && this.state.content[lang];
        return (
            <>
                <div className="container-fluid"
                     style={lang === 'HE' ? {direction: 'rtl', textAlign: 'right'} : null}>
                    <div className="row">
                        <div className="col-12 Hotel-header">
                            <div className="container">
                                <Header
                                    lang={lang}
                                    locale={locale}
                                    changeLang={this.props.changeLang}
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row"
                         style={lang === 'HE' ? {direction: 'rtl', textAlign: 'right'} : null}>
                        <div className="col-12 Hotel-content">
                            <div dangerouslySetInnerHTML={{__html: content}}/>
                            <img src="../../../../media/images/hotel_page_images/1.jpg"/>
                            <br/>
                            {locale.hotel_page_1[lang]}
                            <br/>
                            <img src="../../../../media/images/hotel_page_images/2.jpg"/>
                            <br/>
                            {locale.hotel_page_2[lang]}
                            <br/>
                            <img src="../../../../media/images/hotel_page_images/3.jpg"/>
                            <br/>
                            {locale.hotel_page_3[lang]}
                            <br/>
                            <img src="../../../../media/images/hotel_page_images/4.jpg"/>
                            <br/>
                            {locale.hotel_page_4[lang]}
                            <br/>
                            <img src="../../../../media/images/hotel_page_images/5.jpg"/>
                            <br/>
                            <br/>
                            <img src="../../../../media/images/hotel_page_images/6.jpg"/>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}
