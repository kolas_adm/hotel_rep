import React from 'react';
import HamburgerMenu from 'react-hamburger-menu';
import { Link } from "react-router-dom";

import LangSwitcher from './LangSwitcher/LangSwitcher';
import './Header.css';


export default class Header extends React.Component {
    state = {
        open: false,
    };

    render() {
        const { lang, locale } = this.props;
        const { open } = this.state;
        return (
            <header className='row justify-content-center Header_3-wrapper'>
                <div className="Header_3-back" />
                <div className=' col-sm-12 Header_3'>
                    <div className="row justify-content-center">
                        <Link to='/'
                              className='Header_3-logo'
                              style={lang === 'HE' ? { float: 'right' } : null}
                        >{locale.hotelName[lang]}</Link>
                    </div>

                    <div
                        className='Hamburger-menu'
                        style={{ float: 'left' }}
                    >
                        <HamburgerMenu
                            isOpen={open}
                            menuClicked={() => this.setState({ open: !open })}
                            width={36}
                            height={16}
                            strokeWidth={2}
                            rotate={0}
                            color='white'
                            borderRadius={0}
                            animationDuration={0.5}
                        />
                    </div>
                    <div className="row justify-content-center">
                        <ul className={`Header_3-list ${open ? 'Header-list--mobile' : ''}`}>
                            <li className='Header_3-listItem'><Link to="/booking">{locale.book_now[lang]}</Link></li>
                            <li className='Header_3-listItem blink_stocks'><Link
                                to='/stocks'>{locale.stocks[lang]}</Link></li>
                            <li className='Header_3-listItem'><Link to="/hotel">{locale.hotel[lang]}</Link></li>
                            <li className='Header_3-listItem'><Link to="/restaurant">{locale.restaurant[lang]}</Link>
                            </li>
                            <li className='Header_3-listItem'><Link to='/gallery'>{locale.gallery[lang]}</Link></li>
                            <li className='Header_3-listItem'><Link to='/news'>{locale.news[lang]}</Link></li>
                            <li className='Header_3-listItem'><Link to='/comments'>{locale.comments[lang]}</Link></li>
                            <li className='Header_3-listItem'><Link to='/history'>{locale.history[lang]}</Link></li>
                            <li className='Header_3-listItem'><Link to="/plan">{locale.hotel_plan_page_1[lang]}</Link>
                            </li>
                            <li className='Header_3-listItem'><Link to="/contacts">{locale.contacts[lang]}</Link></li>
                            <LangSwitcher {...this.props} />
                        </ul>
                    </div>

                </div>
            </header>
        );
    }
}
