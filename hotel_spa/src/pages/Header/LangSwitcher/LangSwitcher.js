import React from 'react';

import './LangSwitcher.css';

const langs = ['UA', 'EN', 'HE'];


const LangSwitcher = ({lang, changeLang}) => (
    <li className='LangSwitcher-wrapper'>
        <span><b>{lang}</b></span>
        <div className='LangSwitcher-selector'>
            {langs.map(l => (
                l !== lang && (
                    <div
                        className='LangSwitcher-changer'
                        onClick={() => changeLang(l)}
                    >
                        <span>{l}</span>
                    </div>
                )
            ))}
        </div>
    </li>
);


export default LangSwitcher;
