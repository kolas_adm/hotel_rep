import React from 'react';

import './Restaurant.css';
import "react-image-gallery/styles/css/image-gallery.css";
import ImageGallery from 'react-image-gallery';
import Header from "../Header/Header";

export default class Restaurant extends React.PureComponent {
    state = {
        images: [],
    };

    componentDidMount() {
        fetch('/gallery/carousel/2/')
            .then(response => response.json())
            .then(response => {
                const images = response.map(item => ({ original: item.image, thumbnail: item.image }));
                this.setState({ images: images });
            });
    }

    render() {
        const { images } = this.state;
        const { locale, lang } = this.props;
        return (
            <>
                <div className="container-fluid"
                     style={lang === 'HE' ? { direction: 'rtl', textAlign: 'right' } : null}>
                    <div className="row">
                        <div className="col-12 Restaurant-header">
                            <div className="container">
                                <Header
                                    lang={lang}
                                    locale={locale}
                                    changeLang={this.props.changeLang}
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className='container Restaurant-wrapper'>
                    <div className='row'>
                        <div className="col-sm-12">
                            <ImageGallery
                                items={images}
                                thumbnailPosition="top"
                                showPlayButton={false}
                            />
                        </div>
                        <br/>
                        <div className="col-sm-12">
                            <span>{locale.restaurant_text1[lang]}</span>
                            <br/>
                            <span>{locale.restaurant_text2[lang]}</span>
                            <br/>
                            <span>{locale.restaurant_text3[lang]}</span>
                            <br/>
                            <span>{locale.restaurant_text4[lang]}</span>
                            <br/>
                            <span>{locale.restaurant_text5[lang]}</span>
                            <br/>
                            <span>{locale.restaurant_text6[lang]}</span>
                            <br/>
                            <span>{locale.restaurant_text7[lang]}</span>
                            <br/>
                            <span>{locale.restaurant_text8[lang]}</span>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}
