import React, { PureComponent } from 'react';
import Header from '../Header/Header';
import Room from "../Booking/Room/Room";
import ModalWindow from './ModalWindow/ModalWindow';
import Calendar from 'react-calendar';
import Modal from 'react-responsive-modal';

import './Booking.css';

export default class Booking extends PureComponent {
    state = {
        rooms: null,
        modalIsOpen: false,
        arrDate: null,
        leftDate: null,
        selectedRoom: {},
    };

    makeSearch = (dates) => {
        const arrDate = dates[0].toLocaleDateString('uk');
        const leftDate = dates[1].toLocaleDateString('uk');
        this.setState({ arrDate, leftDate });
        let url = `/rooms/booking/get/${arrDate}/${leftDate}/`;
        fetch(url)
            .then(response => response.json())
            .then(response => {
                const rooms = [];
                const twoBedrooms = response.filter(room => room.room_count === '2');
                const threeBedrooms = response.filter(room => room.room_count === '3');
                if (twoBedrooms.length) {
                    const room = twoBedrooms[0];
                    rooms.push({
                        id: room.id,
                        rooms_count: 2,
                        price: room.price,
                        image: room.image,
                        number: room.room_number,
                        price_two_people: room.price_two_people,
                        price_three_people: 0,

                    });
                }
                if (threeBedrooms.length) {
                    const room = threeBedrooms[0];
                    rooms.push({
                        id: room.id,
                        rooms_count: 3,
                        price: room.price,
                        image: room.image,
                        number: room.room_number,
                        price_two_people: room.price_two_people,
                        price_three_people: room.price_three_people,
                    });
                }
                this.setState({ rooms });
            })
            .catch(err => console.log(err));
    };

    getCalendarLocale = () => {
        switch (this.props.lang) {
            case 'UA':
                return 'uk-UA';
            case 'HE':
                return 'he';
            default:
                return 'en-US';
        }
    };

    render() {
        const { rooms, modalIsOpen } = this.state;
        const { lang, locale } = this.props;
        return (
            <>
                <div className="container-fluid"
                     style={lang === 'HE' ? { direction: 'rtl', textAlign: 'right' } : null}>
                    <div className="row">
                        <div className="col-12 Booking-header">
                            <div className="container">
                                <Header
                                    lang={lang}
                                    locale={locale}
                                    changeLang={this.props.changeLang}
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container Booking-wrapper"
                     style={lang === 'HE' ? { direction: 'rtl', textAlign: 'right' } : null}>
                    <Modal
                        open={modalIsOpen}
                        onClose={() => this.setState({ modalIsOpen: false })}
                        center
                        styles={{
                            modal: {
                                width: '100%',
                                maxWidth: '800px',
                            },
                        }}
                    >
                        <ModalWindow
                            lang={lang}
                            locale={locale}
                            {...this.state}
                        />
                    </Modal>
                    <div className="row Booking-filter">
                        <div className="col-md-4">
                            <div>
                                <Calendar
                                    minDate={new Date(new Date().getTime() + (24 * 60 * 60 * 1000))}
                                    calendarType="ISO 8601"
                                    className={lang !== 'HE' ? 'hotel-calendar' : 'hotel-calendar-he'}
                                    returnValue='range'
                                    selectRange={true}
                                    locale={this.getCalendarLocale()}
                                    onChange={this.makeSearch}
                                />
                            </div>
                        </div>
                        <div className="col-md-8">
                            <Rooms
                                rooms={rooms}
                                lang={lang}
                                locale={locale}
                                openModal={(room) => this.setState({ modalIsOpen: true, selectedRoom: room })}
                            />
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

const Rooms = ({ rooms, lang, locale, openModal }) => {
    if (rooms === null) {
        return (<p className='Booking-text'>{locale.selectRoom[lang]}</p>);
    } else if (!rooms.length) {
        return (<p className='Booking-text'>{locale.noRooms[lang]}</p>);
    }
    return (rooms.map(room => <Room key={room.id} lang={lang} room={room} locale={locale} openModal={openModal} />));
};
