import React, {PureComponent} from 'react';

import './Room.css';

export default class Room extends PureComponent {
    state = {
        showBooking: false,
    };

    toggle = () => this.setState((prevState) => ({showBooking: !prevState.showBooking}));

    renderThreeRoomsPrice() {
        if (this.props.room.price_three_people !== 0)
            return <div className="col-sm-6">
                <p className='Room-price'
                   style={{textAlign: this.props.lang === 'HE' ? 'left' : 'right'}}
                > {this.props.room.price_three_people} <sup>$</sup></p>
            </div>
        return null;
    }

    renderThreeRoomsText() {
        if (this.props.room.rooms_count === 3)
            return <div className="col-sm-6">
                <ul className='Room-options'>
                    <li>Price for three people</li>
                </ul>
            </div>
        return null;
    }

    render() {
        const {room, lang, locale} = this.props;
        return (
            <div className='Room-wrapper' onClick={() => this.props.openModal(room)}>
                <div className="row">
                    <div className="col-sm-4">
                        <img src={room.image} alt="room-img"
                             className='Room-img'/>
                    </div>
                    <div className="col-sm-8">
                        <div className="row">
                            <div className="col-sm-6">
                                <ul className='Room-options'>
                                    <li>{room.rooms_count} {locale.beds[lang]}</li>
                                </ul>
                            </div>
                            <div className="col-sm-6">
                                <p className='Room-price'
                                   style={{textAlign: lang === 'HE' ? 'left' : 'right'}}
                                > {room.price} <sup>$</sup></p>
                            </div>
                            <div className="col-sm-6">
                                <ul className='Room-options'>
                                    <li>Price for two people</li>
                                </ul>
                            </div>
                            <div className="col-sm-6">
                                <p className='Room-price'
                                   style={{textAlign: lang === 'HE' ? 'left' : 'right'}}
                                > {room.price_two_people} <sup>$</sup></p>
                            </div>
                            {this.renderThreeRoomsText()}
                            {this.renderThreeRoomsPrice()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
