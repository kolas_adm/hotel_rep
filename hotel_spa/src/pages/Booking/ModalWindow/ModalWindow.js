import React, {PureComponent} from 'react';

import './ModalWindow.css';
import "react-image-gallery/styles/css/image-gallery.css";
import ImageGallery from 'react-image-gallery';

export default class ModalWindow extends PureComponent {
    state = {
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        comment: '',
        person_count: '',
        country: '',
        showGallery: true,
        isBooking: false,
        isBooked: null,
        images: [],
        continueBooking: false,
    };

    componentDidMount() {
        let url = `/rooms/rooms/images/${this.props.selectedRoom.id}/`;
        fetch(url)
            .then(response => response.json())
            .then(response => {
                const images = response.map(item => ({original: item.image, thumbnail: item.image}));
                this.setState({images: images});

            });
    }

    onSubmitForm = (e) => {
        e.preventDefault();
        const data = {
            "room": this.props.selectedRoom.number,
            "arrival_date": this.props.arrDate,
            "departure_date": this.props.leftDate,
            "comment": this.state.comment,
            "person_count": this.state.person_count,
            "first_name": this.state.firstName,
            "last_name": this.state.lastName,
            "phone": this.state.phone,
            "country": this.state.country,
            "email": this.state.email,
        };
        console.log(data);
        fetch('/rooms/booking/create/', {
            method: 'post',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(data),
        })
            .then((resp) => {
                if (resp.status === 200) {
                    this.setState({isBooking: false, isBooked: true});
                } else {
                    this.setState({isBooking: false, isBooked: false});
                }
            })
            .catch(err => console.log(err));
    };

    render() {
        const {lang, locale, selectedRoom} = this.props;
        const {
            firstName, lastName, email, phone, comment, person_count, country, isBooked, images, showGallery
        } = this.state;
        if (isBooked) {
            return (
                <div className="modal-wrapper">
                    <div className="row">
                        <div className='col-sm-12'>
                            <p>{locale.isBooked[lang]}</p>
                        </div>
                    </div>
                </div>
            );
        }
        if (isBooked === false) {
            return (
                <div className="modal-wrapper">
                    <div className="row">
                        <div className='col-sm-12'>
                            <p>{locale.isBookedError[lang]}</p>
                        </div>
                    </div>
                </div>
            );
        }
        if (showGallery) {
            return (
                <div className='GalleryModal-wrapper'>
                    <div className='row'>
                        <div className="col-sm-12">
                            <ImageGallery
                                items={images}
                                thumbnailPosition="top"
                                showPlayButton={false}
                            />
                        </div>
                        <div className="col-sm-12">
                            <button
                                className='GalleryModal-button'
                                onClick={() => this.setState({showGallery: false})}
                            >{locale.booking[lang]}</button>
                        </div>
                    </div>
                </div>
            )
        }
        return (
            <form>
                <div className="modal-wrapper">
                    <div className="row">
                        <div className='col-sm-6'>
                            <input
                                required
                                type="text"
                                defaultValue={lastName}
                                placeholder={locale.lastName[lang]}
                                style={lang === 'HE' ? {direction: 'rtl', textAlign: 'right'} : null}
                                onChange={(e) => this.setState({lastName: e.target.value})}
                            />
                        </div>
                        <div className='col-sm-6'>
                            <input
                                required
                                type="text"
                                defaultValue={firstName}
                                placeholder={locale.firstName[lang]}
                                style={lang === 'HE' ? {direction: 'rtl', textAlign: 'right'} : null}
                                onChange={(e) => this.setState({firstName: e.target.value})}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className='col-sm-12'>
                            <input
                                required
                                type="email"
                                defaultValue={email}
                                placeholder={locale.email[lang]}
                                style={lang === 'HE' ? {direction: 'rtl', textAlign: 'right'} : null}
                                onChange={(e) => this.setState({email: e.target.value})}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className='col-sm-12'>
                            <input
                                required
                                type="text"
                                defaultValue={phone}
                                placeholder={locale.phone[lang]}
                                style={lang === 'HE' ? {direction: 'rtl', textAlign: 'right'} : null}
                                onChange={(e) => this.setState({phone: e.target.value})}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className='col-sm-12'>
                            <input
                                required
                                type="number"
                                defaultValue={person_count}
                                max={selectedRoom.rooms_count}
                                placeholder={locale.personsCount[lang]}
                                style={lang === 'HE' ? {direction: 'rtl', textAlign: 'right'} : null}
                                onChange={(e) => this.setState({person_count: e.target.value})}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className='col-sm-12'>
                            <input
                                type="text"
                                defaultValue={country}
                                placeholder={locale.country[lang]}
                                style={lang === 'HE' ? {direction: 'rtl', textAlign: 'right'} : null}
                                onChange={(e) => this.setState({country: e.target.value})}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className='col-sm-12'>
                        <textarea
                            placeholder={locale.comment[lang]}
                            defaultValue={comment}
                            style={lang === 'HE' ? {direction: 'rtl', textAlign: 'right'} : null}
                            onChange={(e) => this.setState({comment: e.target.value})}
                        />
                        </div>
                    </div>
                    <div className="row">
                        <div className='col-sm-12'>
                            <button
                                disabled={this.state.isBooking}
                                onClick={(e) => this.setState({isBooking: true}, () => this.onSubmitForm(e))}
                            >{locale.booking[lang]}</button>
                        </div>
                    </div>
                </div>
            </form>
        );
    };
}
