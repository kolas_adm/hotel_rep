import React from 'react';

import './Gallery.css';
import "react-image-gallery/styles/css/image-gallery.css";
import ImageGallery from 'react-image-gallery';
import Header from "../Header/Header";

export default class Gallery extends React.PureComponent {
    state = {
        images: [],
    };

    componentDidMount() {
        fetch('/gallery/carousel/1/')
            .then(response => response.json())
            .then(response => {
                const images = response.map(item => ({ original: item.image, thumbnail: item.image }));
                this.setState({ images: images });

            });
    }

    render() {
        const { images } = this.state;
        const { locale, lang } = this.props;
        return (
            <>
                <div className="container-fluid"
                     style={lang === 'HE' ? { direction: 'rtl', textAlign: 'right' } : null}>
                    <div className="row">
                        <div className="col-12 Gallery-header">
                            <div className="container">
                                <Header
                                    lang={lang}
                                    locale={locale}
                                    changeLang={this.props.changeLang}
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className='container Gallery-wrapper'>
                    <div className='row'>
                        <div className="col-sm-12">
                            <ImageGallery
                                items={images}
                                thumbnailPosition="top"
                                showPlayButton={false}
                            />
                        </div>
                    </div>
                </div>
            </>
        );
    }
}
