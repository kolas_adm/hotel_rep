import React from 'react';

import Header from '../Header/Header';
import './History.css';


export default class History extends React.Component {
    state = {
        content: null,
    };

    componentDidMount() {
        fetch('/posts/page/history/')
            .then(response => response.json())
            .then(response => this.setState({ content: response }));
    }

    render() {
        const { lang, locale } = this.props;
        const content = this.state.content && this.state.content[lang];
        return (
            <>
                <div className="container-fluid"
                     style={lang === 'HE' ? { direction: 'rtl', textAlign: 'right' } : null}>
                    <div className="row">
                        <div className="col-12 History-header">
                            <div className="container">
                                <Header
                                    lang={lang}
                                    locale={locale}
                                    changeLang={this.props.changeLang}
                                />
                                <div className="row">
                                    <h1 className='History-header-text'>{locale.umanHistory[lang]}</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row"
                         style={lang === 'HE' ? { direction: 'rtl', textAlign: 'right' } : null}>
                        <div className="col-12 History-content">
                            <div dangerouslySetInnerHTML={{ __html: content }} />
                        </div>
                    </div>
                </div>
            </>
        );
    }
}