import React from 'react';
import './NewsList.css';
import Header from '../Header/Header';
import NewsItem from './NewsItem';


export default class NewsList extends React.PureComponent {
    state = {
        results: [],
        page: 1,
    };

    componentDidMount() {
        document.addEventListener('scroll', this.trackScrolling);
        fetch('/rooms/news/')
            .then(response => response.json())
            .then(response => this.setState({results: response.results, page: response.next ? 1 : null}));
    }

    componentWillUnmount() {
        document.removeEventListener('scroll', this.trackScrolling);
    }

    updateNews = () => {
        const page = this.state.page + 1;
        fetch(`/rooms/news/?page=${page}`)
            .then(response => response.json())
            .then(response => this.setState({
                results: [...this.state.results, ...response.results],
                page: response.next ? page : null,
            }));
    };

    trackScrolling = () => {
        const wrappedElement = document.getElementById('NewsList-list');

        const isBottom = (el) => el.getBoundingClientRect().bottom <= window.innerHeight;

        if (isBottom(wrappedElement) && this.state.page) {
            this.updateNews();
        }
    };

    render() {
        const {results} = this.state;
        const {locale, lang} = this.props;
        return (
            <>
            <div className="container-fluid"
                 style={lang === 'HE' ? {direction: 'rtl', textAlign: 'right'} : null}>
                <div className="row">
                    <div className="col-12 NewsList-header">
                        <div className="container">
                            <Header
                                lang={lang}
                                locale={locale}
                                changeLang={this.props.changeLang}
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div className="container" id='NewsList-list'
                 style={lang === 'HE' ? {direction: 'rtl', textAlign: 'right'} : null}>
                <div className="row">
                    <div className="col-12 NewsList-list">
                        {results.map(res => (
                            <NewsItem key={res.id} news={res} lang={lang}/>
                        ))}
                    </div>
                </div>
            </div>
            </>
        )
    }
}