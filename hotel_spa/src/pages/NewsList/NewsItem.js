import React from 'react';

export default class NewsItem extends React.PureComponent {
    state = {
        content: null,
        show: false,
    };

    toggleContent = () => {
        if (this.state.content) {
            this.setState({show: !this.state.show})
        } else {
            fetch(`/rooms/news/${this.props.news.id}/`)
                .then(response => response.json())
                .then(response => this.setState({content: response, show: true}));
        }

    };

    render() {
        const {news, lang} = this.props;
        const {content, show} = this.state;
        return (
            <div className='NewsList-item' onClick={this.toggleContent}>
                <a className='NewsList-item--link'>
                    {news[`title_${lang.toLowerCase()}`]}
                </a>
                <span className='NewsList-item--date'>{news.timestamp}</span>
                {content && show && (
                    <div
                        className='NewsItem-content'
                        dangerouslySetInnerHTML={{__html: content[`description_${lang.toLowerCase()}`]}}
                    />
                )}
            </div>
        )
    }
}
