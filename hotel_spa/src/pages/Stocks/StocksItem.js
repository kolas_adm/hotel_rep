import React from 'react';
import {Link} from "react-router-dom";

export default class StocksItem extends React.PureComponent {
    state = {
        content: null,
        show: false,
    };

    toggleContent = () => {
        // if (this.state.content) {
        //     this.setState({show: !this.state.show})
        // } else {
        //     fetch(`/stocks/stocks/${this.props.stocks.id}/`)
        //         .then(response => response.json())
        //         .then(response => this.setState({content: response, show: true}));
        // }
    };

    render() {
        const {stocks, lang} = this.props;
        const {content, show} = this.state;
        return (
            <div className='StocksList-item' onClick={this.toggleContent}>
                <Link to="/booking">
                    <p className={'StocksList-item--link'}>
                        {stocks[`title_${lang.toLowerCase()}`]}
                    </p>
                    <img height={'400px;'} width={'600px;'} src={stocks.image}/>
                    <br/>
                    <span className='StocksList-item--date'>{stocks.timestamp}</span>
                    <br/>
                    <div
                        dangerouslySetInnerHTML={{__html: stocks[`description_${lang.toLowerCase()}`]}}
                    />
                </Link>
            </div>
        )
    }
}
