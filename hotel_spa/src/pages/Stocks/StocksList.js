import React from 'react';
import './StocksList.css';
import Header from '../Header/Header';
import StocksItem from './StocksItem';


export default class StocksList extends React.PureComponent {
    state = {
        results: [],
        page: 1,
    };

    componentDidMount() {
        document.addEventListener('scroll', this.trackScrolling);
        fetch('/stocks/stocks/')
            .then(response => response.json())
            .then(response => this.setState({ results: response.results, page: response.next ? 1 : null }));
    }

    componentWillUnmount() {
        document.removeEventListener('scroll', this.trackScrolling);
    }

    updateNews = () => {
        const page = this.state.page + 1;
        fetch(`/stocks/stocks/?page=${page}`)
            .then(response => response.json())
            .then(response => this.setState({
                results: [...this.state.results, ...response.results],
                page: response.next ? page : null,
            }));
    };

    changeLang = (lang) => this.setState({ lang }, () => localStorage.setItem('lang', lang));

    trackScrolling = () => {
        const wrappedElement = document.getElementById('StocksList-list');

        const isBottom = (el) => el.getBoundingClientRect().bottom <= window.innerHeight;

        if (isBottom(wrappedElement) && this.state.page) {
            this.updateNews();
        }
    };

    render() {
        const { results } = this.state;
        const { locale, lang } = this.props;
        return (
            <>
                <div className="container-fluid"
                     style={lang === 'HE' ? { direction: 'rtl', textAlign: 'right' } : null}>
                    <div className="row">
                        <div className="col-12 StocksList-header">
                            <div className="container">
                                <Header
                                    lang={lang}
                                    locale={locale}
                                    changeLang={this.props.changeLang}
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container" id='StocksList-list'
                     style={lang === 'HE' ? { direction: 'rtl', textAlign: 'right' } : null}>
                    <div className="row">
                        <div className="col-12 StocksList-list">
                            {results.map(res => (
                                <StocksItem key={res.id} stocks={res} lang={lang} />
                            ))}
                        </div>
                    </div>
                </div>
            </>
        );
    }
}