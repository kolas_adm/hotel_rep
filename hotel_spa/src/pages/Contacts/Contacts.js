import React from 'react';

import Header from '../Header/Header';
import './Contacts.css';

export default class Contacts extends React.Component {
    state = {
        content: null,
    };

    componentDidMount() {
        fetch('/posts/page/hotel/')
            .then(response => response.json())
            .then(response => this.setState({ content: response }));
    }

    render() {
        const { lang, locale } = this.props;
        const content = this.state.content && this.state.content[lang];
        return (
            <>
                <div className="container-fluid"
                     style={lang === 'HE' ? { direction: 'rtl', textAlign: 'right' } : null}>
                    <div className="row">
                        <div className="col-12 Contact-header">
                            <div className="container">
                                <Header
                                    lang={lang}
                                    locale={locale}
                                    changeLang={this.props.changeLang}
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row"
                         style={lang === 'HE' ? { direction: 'rtl', textAlign: 'right' } : null}>
                        <div className="col-12 Hotel-content">
                            <div dangerouslySetInnerHTML={{ __html: content }} />
                            {locale.contacts_page_1[lang]}
                            <br/>
                            {locale.contacts_page_2[lang]}
                            <br/>
                            {locale.contacts_page_3[lang]}
                            <br/>
                            <img src="../../../../media/images/contacts_images/map.jpg"/>
                            <br/>
                            {locale.contacts_page_4[lang]}
                            <br/>
                            {locale.contacts_page_5[lang]}
                            <br/>
                            {locale.contacts_page_6[lang]}
                            <br/>
                            {locale.contacts_page_7[lang]}
                            <br/>
                            <br/>
                            <br/>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}
