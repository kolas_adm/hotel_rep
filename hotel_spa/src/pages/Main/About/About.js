import React from 'react';

import './About.css';
import hall from './hot.jpeg';
import rest from '../../../images/rest/rest.jpg'
import { Link } from "react-router-dom";

const About = ({ lang, locale }) => (
    <div className='container' style={lang === 'HE' ? { direction: 'rtl', textAlign: 'right' } : null}>
        <div id="hotel" className="row justify-content-center About-wrapper">
            <div className="col-sm-5">
                <Link to="/hotel">

                    <h2 className='About-header'>{locale.hotel[lang]}</h2>
                    <p className='About-text'>{locale.aboutText[lang]}</p>
                </Link>

            </div>
            <div className="col-sm-7">
                <Link to="/hotel">
                    <img src={hall} alt="hotel-room" className='About-img' />
                </Link>
            </div>
        </div>
        <div id="restaurant" className="row justify-content-center About-wrapper">
            <div className="col-sm-5">
                <Link to="/restaurant">
                    <img src={rest} alt="restaurant"
                         className='About-img' />
                </Link>
            </div>
            <div className="col-sm-7">
                <Link to="/restaurant">
                    <h2 className='About-header'>{locale.restaurant[lang]}</h2>
                </Link>
                <p className='About-text'>{locale.aboutRestaurantText[lang]}</p>
            </div>
        </div>
    </div>
);

export default About;
