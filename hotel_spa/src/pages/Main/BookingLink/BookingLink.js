import React from 'react';
import {Link} from "react-router-dom";

import './BookingLink.css';


const BookingLink = ({lang, locale}) => (
    <div className="row">
        <div className="col-md-5 BookingLink-wrapper">
            <Link to='/booking'>{locale.bookRoomButton[lang]}</Link>
        </div>
    </div>

);

export default BookingLink;
