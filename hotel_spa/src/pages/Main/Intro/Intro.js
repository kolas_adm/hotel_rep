import React from 'react';

import './Intro.css';


const Intro = ({lang, locale}) => (
    <div className='Intro-wrapper'>
        <div className="row">
            <div className="col-md-5">
                <h1 className='Intro-header'>{locale.welcome[lang]}<br/>
                    <span className='Intro-hotelname'>{locale.hotelName[lang]}</span>
                </h1>
            </div>
        </div>

        <div className="row">
            <div className="col-md-5">
                <p className='Intro-subtext'>
                    {locale.subIntroText[lang]}
                </p>
            </div>
        </div>
    </div>
);

export default Intro;
