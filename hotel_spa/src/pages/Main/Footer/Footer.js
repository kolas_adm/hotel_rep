import React from 'react';

import './Footer.css';

const Footer = ({lang, locale}) => (
    <div className='Footer-wrapper' style={lang === 'HE' ? {direction: 'rtl', textAlign: 'right'} : null}>
        <div className="container">
            <div className="row justify-content-between">
                <div className="col-sm-4 Footer-address" style={{direction: 'ltr'}}>
                    <p><b>{locale.hotelName[lang]}</b></p>
                    <p>{locale.addressStreet[lang]}</p>
                    <p>{locale.addressCity[lang]}</p>
                    <br/>
                    <p>+38 093 481 48 93</p>
                    <p>uman.melania@gmail.com</p>
                </div>
                <div className="col-sm-2">
                    <div className='Footer-menu'>
                        <div className='Footer-menuItem'>
                            <a href="#hotel">{locale.hotel[lang]}</a>
                        </div>
                        <div className='Footer-menuItem'>
                            <a href="#restaurant">{locale.restaurant[lang]}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
);

export default Footer;