import React from 'react';

import LangSwitcher from './LangSwitcher/LangSwitcher';
import HamburgerMenu from 'react-hamburger-menu';
import {Link} from "react-router-dom";
import './Header.css';


export default class Header extends React.PureComponent {
    state = {
        open: false,
    };

    render() {
        const {lang, locale} = this.props;
        const {open} = this.state;
        return (
            <header className='row justify-content-center Header-wrapper'>
                <div className='col-sm-12 Header'>
                    <div className="row justify-content-center" >
                        <Link to='/'
                       className='Header-logo'
                       style={lang === 'HE' ? {float: 'right'} : null}
                    >{locale.hotelName[lang]}</Link>
                    </div>
                    <div
                        className='Hamburger-menu'
                        style={{float: 'left'}}
                    >
                        <HamburgerMenu
                            isOpen={open}
                            menuClicked={() => this.setState({open: !open})}
                            width={36}
                            height={16}
                            strokeWidth={2}
                            rotate={0}
                            color='black'
                            borderRadius={0}
                            animationDuration={0.5}
                        />
                    </div>
                    <div className="row justify-content-center">
                        <ul className={`Header-list ${open ? 'Header-list--mobile' : ''}`}>
                        <li className='Header-listItem'><Link to="/booking">{locale.book_now[lang]}</Link></li>
                        <li className='Header-listItem blink_stocks'><Link to='/stocks'>{locale.stocks[lang]}</Link></li>
                        <li className='Header-listItem'><Link to="/hotel">{locale.hotel[lang]}</Link></li>
                        <li className='Header-listItem'><Link to="/restaurant">{locale.restaurant[lang]}</Link></li>
                        <li className='Header-listItem'><Link to='/gallery'>{locale.gallery[lang]}</Link></li>
                        <li className='Header-listItem'><Link to='/news'>{locale.news[lang]}</Link></li>
                        <li className='Header-listItem'><Link to='/comments'>{locale.comments[lang]}</Link></li>
                        <li className='Header-listItem'><Link to='/history'>{locale.history[lang]}</Link></li>
                        <li className='Header-listItem'><Link to="/plan">{locale.hotel_plan_page_1[lang]}</Link></li>
                        <li className='Header-listItem'><Link to="/contacts">{locale.contacts[lang]}</Link></li>
                        <LangSwitcher {...this.props} />
                    </ul>
                    </div>

                </div>
            </header>
        )
    }
}
