import React, { PureComponent } from 'react';

import Header from './Header/Header';
import Intro from './Intro/Intro';
import BookingLink from './BookingLink/BookingLink';
import About from './About/About';
import Footer from './Footer/Footer';
import './Main.css';


export default class Main extends PureComponent {
    render() {
        return (
            <>
                <div className='Main-wrapper'
                     style={this.props.lang === 'HE' ? { direction: 'rtl', textAlign: 'right' } : null}
                >
                    <div className="container">
                        <Header
                            {...this.props}
                            changeLang={this.props.changeLang}
                        />
                        <Intro {...this.props} />
                        <BookingLink {...this.props} />
                    </div>
                </div>
                <About {...this.props} />
                <Footer {...this.props} />
            </>
        );
    }
}