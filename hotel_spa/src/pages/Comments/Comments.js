import React from 'react';
import Header from '../Header/Header';
import SocialAuth from './SocialAuth';
import './Comments.css';
import Stars from './Stars';
import CommentForm from './CommentForm';


export default class Comments extends React.PureComponent {
    state = {
        comments: [],
        showForm: false,
        isAuthed: false,
    };

    componentDidMount() {
        this.setState({ isAuthed: this.checkAuth() });
        fetch('/posts/posts/')
            .then(response => response.json())
            .then(response => this.setState({ comments: response }));
    }

    onAuthLogin = (res, backend) => {
        const token = res.accessToken;
        const data = {
            "grant_type": "convert_token",
            "client_id": "8QQo8Kac47Xe1QoMMvYdohNYun67k0KYZTlAQ7uw",
            "client_secret": "pTnT813HfBwcaEfVbRJSq1e512dFSdjGeFuuufCZsV7Yp3nMa7uA3fffClv01atzXtlgNF7NpbPbCgu8bTqtMgTvJoJXoTUg2kYtS28znFQCBDHU5rggjaMG8zCCqq2U",
            "backend": backend,
            "token": token,
        };
        fetch('/auth/social/convert-token/', {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data),
        })
            .then(res => res.json())
            .then(res => {
                const expiryDate = Math.round(new Date().getTime()) + ((res.expires_in - 60 * 60 * 48) * 1000);
                localStorage.setItem('auth_token', res.access_token);
                localStorage.setItem('expiryDate', expiryDate);
                this.setState({ showForm: true, isAuthed: true });
            })
            .catch(err => console.log(err, 'error'));
    };

    addComment = (comment) => this.setState({ comments: [comment, ...this.state.comments], showForm: false });

    checkAuth = () => {
        const token = localStorage.getItem('auth_token');
        if (!token) {
            return false;
        }
        const tokenExpiryDate = localStorage.getItem('expiryDate');
        const now = new Date().getTime();
        return tokenExpiryDate > now;
    };

    render() {
        const { comments, showForm, isAuthed } = this.state;
        const { lang, locale } = this.props;
        return (
            <>
                <div className="container-fluid"
                     style={lang === 'HE' ? { direction: 'rtl', textAlign: 'right' } : null}>
                    <div className="row">
                        <div className="col-12 Comments-header">
                            <div className="container">
                                <Header
                                    lang={lang}
                                    locale={locale}
                                    changeLang={this.props.changeLang}
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container Comments-wrapper"
                     style={lang === 'HE' ? { direction: 'rtl', textAlign: 'right' } : null}
                >
                    <div className="row justify-content-end">
                        {showForm &&
                        <div className="col-12">
                            {isAuthed ? <CommentForm lang={lang} locale={locale} addComment={this.addComment} /> :
                                <SocialAuth lang={lang} locale={locale} onAuthLogin={this.onAuthLogin} />}
                        </div>}
                        {!showForm &&
                        <div className="col-lg-3">
                            <button className='Comments-add-form'
                                    onClick={() => this.setState({ showForm: true })}
                            >{locale.add[lang]}</button>
                        </div>}
                    </div>
                    <div className="row">
                        <div className="col-12">
                            {comments.map(comm => (
                                <div className='Comments-row'>
                                    <p><b>{comm.full_name_user}</b> <span className='Comments-stars'><Stars
                                        rating={comm.rating_status} /></span></p>
                                    <p>{comm.description}</p>
                                    <p><b>{locale.advantages[lang]}:</b> {comm.advantages}</p>
                                    <p><b>{locale.limitations[lang]}:</b> {comm.limitations}</p>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </>
        );
    }
}