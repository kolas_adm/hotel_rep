import React from 'react';

import goldStar from './start_gold.png';
import greyStar from './start_grey.png';


const Stars = ({rating}) => (
    [1, 2, 3, 4, 5].map(point => (
        <img src={rating < point ? greyStar : goldStar} alt="star"/>
    ))
);

export default Stars;
