import React from 'react';

import FacebookLogin from "react-facebook-login";
import GoogleLogin from "react-google-login";

const SocialAuth = ({lang, locale, onAuthLogin}) => (
    <div className='row justify-content-start'
         style={lang === 'HE' ? { direction: 'rtl', textAlign: 'right' } : null}>
        <div className="col">
            <div className="row justify-content-center">
                <p>{locale.authPlease[lang]}</p>
                <div className="social-button-wrapper">
                    <FacebookLogin
                        appId="244511323097550"
                        fields='id, name, email'
                        textButton={locale.login[lang]}
                        callback={(res) => onAuthLogin(res, 'facebook')}
                        cssClass="my-facebook-button-class"
                        icon="fa-facebook"
                    />
                </div>
                <div className="social-button-wrapper">
                    <GoogleLogin
                        clientId="515085949765-sav9tl89qct1jct6aokbbajhceourdhb.apps.googleusercontent.com"
                        buttonText={locale.login[lang]}
                        onSuccess={(res) => onAuthLogin(res, 'google-oauth2')}
                        onFailure={(res) => console.log(res)}
                        prompt="select_account"
                    />
                </div>
            </div>
        </div>
    </div>
);

export default SocialAuth;
