import React from 'react';

export default class CommentForm extends React.PureComponent {
    state = {
        comment: '',
        advantages: '',
        limitations: '',
        rating_status: '5',
    };
    onSubmitForm = (e) => {
        e.preventDefault();
        const { comment, advantages, limitations, rating_status } = this.state;
        const token = localStorage.getItem("auth_token");
        fetch('/posts/posts/create/', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify({ description: comment, advantages, limitations, rating_status }),
        })
            .then(res => res.json())
            .then(res => this.props.addComment(res))
            .catch(err => console.log(err))
    };

    render() {
        const { lang, locale } = this.props;
        const { comment, advantages, limitations, rating_status } = this.state;
        return (
            <form className='row'>
                <div className="col-12"
                     style={lang === 'HE' ? { direction: 'rtl', textAlign: 'right' } : null}
                >
                    <span>{locale.rating[lang]}:</span>
                    {['1', '2', '3', '4', '5'].map(point => (
                        <div className="points_input" key={point}>
                            <input
                                type="radio"
                                name="points"
                                value={point}
                                checked={rating_status === point}
                                onChange={() => this.setState({rating_status: point})}
                            /> {point}
                        </div>
                    ))}
                </div>
                <div className="col-12 ">
                    <textarea
                        className='Comment-textarea'
                        placeholder={locale.comment[lang]}
                        defaultValue={comment}
                        style={lang === 'HE' ? { direction: 'rtl', textAlign: 'right' } : null}
                        onChange={(e) => this.setState({ comment: e.target.value })}
                    />
                </div>
                <div className="col-6">
                    <textarea
                        className='Comment-input'
                        placeholder={locale.advantages[lang]}
                        defaultValue={advantages}
                        style={lang === 'HE' ? { direction: 'rtl', textAlign: 'right' } : null}
                        onChange={(e) => this.setState({ advantages: e.target.value })}
                    />
                </div>
                <div className="col-6">
                    <textarea
                        className='Comment-input'
                        placeholder={locale.limitations[lang]}
                        defaultValue={limitations}
                        style={lang === 'HE' ? { direction: 'rtl', textAlign: 'right' } : null}
                        onChange={(e) => this.setState({ limitations: e.target.value })}
                    />
                </div>
                <div className="col-12">
                    <button onClick={this.onSubmitForm} className='Comments-add-form'>{locale.add[lang]}</button>
                </div>
            </form>
        );
    }
}
