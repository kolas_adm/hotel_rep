import React from 'react';

import Header from '../Header/Header';
import './HotelPlan.css';
import {Link} from "react-router-dom";

export default class HotelPlan extends React.Component {
    state = {
        content: null,
    };

    componentDidMount() {
        fetch('/posts/page/hotel/')
            .then(response => response.json())
            .then(response => this.setState({content: response}));
    }

    render() {
        const {lang, locale} = this.props;
        const content = this.state.content && this.state.content[lang];
        return (
            <>
                <div className="container-fluid"
                     style={lang === 'HE' ? {direction: 'rtl', textAlign: 'right'} : null}>
                    <div className="row">
                        <div className="col-12 HotelPlan-header">
                            <div className="container">
                                <Header
                                    lang={lang}
                                    locale={locale}
                                    changeLang={this.props.changeLang}
                                />
                                <div className="row">
                                    <h1 className='HotelPlan-header-text'>{locale.hotel[lang]} {locale.hotelName[lang]}</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row"
                         style={lang === 'HE' ? {direction: 'rtl', textAlign: 'right'} : null}>
                        <div className="col-8 HotelPlan-content">
                            <div dangerouslySetInnerHTML={{__html: content}}/>
                            <br/>
                            <img src="../../../../media/images/hotel_plan_images/1store.jpg"/>
                            <br/>
                            <br/>
                            <img src="../../../../media/images/hotel_plan_images/2store.jpg"/>
                            <br/>
                            <br/>
                            <img src="../../../../media/images/hotel_plan_images/2store.jpg"/>
                            <br/>
                            <br/>
                            <img src="../../../../media/images/hotel_plan_images/2store.jpg"/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                        </div>
                        <div className="col-4 HotelPlan-content">
                            <div className="row">
                                <Link to="/booking">
                                {locale.standart2[lang]}
                                </Link>
                            </div>
                            <div className="row">
                                102
                            </div>
                            <div className="row">
                                103
                            </div>
                            <div className="row">
                                202
                            </div>
                            <div className="row">
                                203
                            </div>
                            <div className="row">
                                302
                            </div>
                            <div className="row">
                                303
                            </div>

                            <div className="row">
                                <Link to="/booking">
                                {locale.standart2[lang]}
                                </Link>
                            </div>
                            <div className="row">
                                101
                            </div>
                            <div className="row">
                                104
                            </div>
                            <div className="row">
                                201
                            </div>
                            <div className="row">
                                204
                            </div>
                            <div className="row">
                                301
                            </div>
                            <div className="row">
                                304
                            </div>
                            <div className="row">
                                <Link to="/booking">
                                {locale.family_room[lang]}
                                </Link>
                            </div>
                            <div className="row">
                                205
                            </div>
                            <div className="row">
                                206
                            </div>
                            <div className="row">
                                305
                            </div>
                            <div className="row">
                                306
                            </div>
                            <div className="row">
                                <Link to="/booking">
                                {locale.lux2[lang]}
                                </Link>
                            </div>
                            <div className="row">
                                402
                            </div>
                            <div className="row">
                                403
                            </div>
                            <div className="row">
                                <Link to="/booking">
                                {locale.lux3[lang]}
                                </Link>
                            </div>
                            <div className="row">
                                401
                            </div>
                            <div className="row">
                                404
                            </div>
                            <div className="row">
                                <Link to="/booking">
                                {locale.penthaus[lang]}
                                </Link>
                            </div>
                            <div className="row">
                                405
                            </div>
                            <div className="row">
                                406
                            </div>
                        </div>

                    </div>
                </div>
                <div className="row"
                     style={lang === 'HE' ? {direction: 'rtl', textAlign: 'right'} : null}>

                </div>

            </>
        );
    }
}
