import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

import Main from './pages/Main/Main';
import Booking from './pages/Booking/Booking';
import History from './pages/History/History';
import Hotel from './pages/Hotel/Hotel';
import Comments from './pages/Comments/Comments';
import NewsList from './pages/NewsList/NewsList';
import StocksList from './pages/Stocks/StocksList';
import Gallery from './pages/Gallery/Gallery';
import Restaurant from './pages/Restaurant/Restaurant';
import Contacts from './pages/Contacts/Contacts';
import HotelPlan from './pages/HotelPlan/HotelPlan';

class App extends Component {
    state = {
        locale: null,
        lang: localStorage.getItem('lang') || 'UA',
    };

    componentDidMount() {
        fetch('/stocks/locales/')
            .then(response => response.json())
            .then(response => this.setState({ locale: response }));
    }

    changeLang = (lang) => this.setState({ lang }, () => localStorage.setItem('lang', lang));

    render() {
        const { state } = this;
        if (state.locale === null) {
            return null;
        }
        return (
            <Router>
                <main>
                    <Route exact path="/" render={() => (
                        <Main {...state} changeLang={this.changeLang} />
                    )} />
                    <Route exact path="/booking" render={() => (
                        <Booking {...state} changeLang={this.changeLang} />
                    )} />
                    <Route exact path="/stocks" render={() => (
                        <StocksList {...state} changeLang={this.changeLang} />
                    )} />
                    <Route exact path="/history" render={() => (
                        <History {...state} changeLang={this.changeLang} />
                    )} />
                    <Route exact path="/news" render={() => (
                        <NewsList {...state} changeLang={this.changeLang} />)} />
                    <Route exact path="/hotel" render={() => (
                        <Hotel {...state} changeLang={this.changeLang} />
                    )} />
                    <Route exact path="/comments" render={() => (
                        <Comments {...state} changeLang={this.changeLang} />
                    )} />
                    <Route exact path="/gallery" render={() => (
                        <Gallery {...state} changeLang={this.changeLang} />
                    )} />
                    <Route exact path="/restaurant" render={() => (
                        <Restaurant {...state} changeLang={this.changeLang} />
                    )} />
                    <Route exact path="/contacts" render={() => (
                        <Contacts {...state} changeLang={this.changeLang} />
                    )} />
                    <Route exact path="/plan" render={() => (
                        <HotelPlan {...state} changeLang={this.changeLang} />
                    )} />
                </main>
            </Router>
        );
    }
}

export default App;
